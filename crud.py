from PyQt5 import QtWidgets, uic
from PyQt5.QtWidgets import *
import sys

aplication = QtWidgets.QApplication([])
ventana = uic.loadUi("ventana.ui")
ventana.show()

ventana.tblContactos.setHorizontalHeaderLabels(['ID', 'Nombre', 'Correo'])
ventana.tblContactos.setEditTriggers(QTableWidget.NoEditTriggers)
ventana.tblContactos.setSelectionBehavior(QTableWidget.SelectRows)

sys.exit(aplication.exec())

